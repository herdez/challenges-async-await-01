/*
Given the following implementation:


//firstResult()
const firstResult = (err, data, callback) => {
    if (err) return callback(err)
    console.log(data)
    slowFunction((err, data) => secondResult(err, data, callback))
}

//secondResult()
const secondResult = (err, data, callback) => {
    if (err) return callback(err)
    console.log(data)
}

//fastFunction()
function fastFunction (done) {
    const a =  "Results of a",
          error = null
          //error = "Error in fastFunction"
    setTimeout(function () {
      done(error, a)
    }, 1000)
}

//slowFunction()
function slowFunction (done) {
    const b = "Results of b",
          error = "Error in slowFunction"
    setTimeout(function () {
      done(error, "Results of b")
    }, 3000)
}
  
// runSequentially()
function runSequentially (callback) {
    fastFunction((err, data) => firstResult(err, data, callback))
}


// driver code
runSequentially((err) => console.log(err))

*/

/*** 
 * We have the challenge to use asynchronous code using 'await' and 'async' and
 * reach the following output:
 *
 * 
 * When 'fastFunction()' & 'slowFunction()' are resolved:
 * 
 * ```
 * [
 *  'Results of a',
 *  'Results of b'
 * ]
 * ``` 
 * 
 * When there's an error in 'fastFunction()':
 * 
 * ```
 * Error in fastFunction
 * ```
 * 
 * All tests must be true.
 *  
 */
 

//+++ YOUR CODE GOES HERE



//fastFunction()




//slowFunction()





//runSequentially()








// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~* Tests (Don't Touch) *~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*

// Uncomment to test

/*
console.log("*~*~*~* When all services are resolved *~*~*~*")

runSequentially()

.then(data => {
    console.log("Test 1...", data.indexOf('Results of a') >= 0)
    console.log("Test 2...", data.indexOf('Results of b') >= 0)
})
 
.catch(e => console.log(e))

*/

/*

console.log("*~*~*~* Error in fastFunction() *~*~*~*")

runSequentially()

.then(data => {
    console.log("Test 1...", data.indexOf('Results of a') >= 0)
    console.log("Test 2...", data.indexOf('Results of b') >= 0)
})
 
.catch(e => console.log("Test 3...", e === "Error in fastFunction"))

*/