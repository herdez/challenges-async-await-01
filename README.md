# NodeJS Challenges - Async/Await

## Objectives:

- How to use `async/await` pattern.


### Theory & Documentation

## Async/Await

`Async` functions are a combination of promises and generators, and basically, they are a higher level abstraction over promises.

> `async/await` is built on promises.

## How it works

Prepending the `async` keyword to any function means that the function will return a `promise`.

Even if it's not doing so explicitly, it will internally make it return a `promise`.

```javascript
const aFunction = async () => {
    return 'test'
  }
  
aFunction()

.then(data => console.log(data))        //test

```

and it's the same as:

```javascript
const aFunction = () => {
  return Promise.resolve('test')
}

aFunction()

.then((data) => data)   //test

```

## An async function returns a promise

An async function returns a promise, like in this example:

```javascript
const doSomethingAsync = () => {
  return new Promise(resolve => {
    setTimeout(() => resolve('I say Jallo'), 3000)
  })
}
```

When you want to call this function you prepend `await`, and the calling code will stop until the promise is `resolved` or `rejected`. 

>The client function must be defined as `async`.

```javascript
const doSomething = async () => {
    return await doSomethingAsync()
}


doSomething()

.then(data => console.log(data))
```

The output is...

```
I say Jallo
```

## Example 1

The following example is with `Async/Await` style:

```javascript
function job() {
    return new Promise(function(resolve, reject) {
        setTimeout(resolve, 500, 'Hello world 1');
    });
}

async function test() {
    let message = await job()
    console.log(message)

    return 'Hello world 2'
}

test()

.then(function(message) {
    console.log(message)
})
```

```
Hello world 1                                                
Hello world 2 
```

The same example with `promise` style:

```javascript
function test() {
    return job().then(message => {
        console.log(message)
                
        return "Hello world 2"
    })
}

test()

.then(message => console.log(message))

```

Let's examine `async/await` example:

The `async` keyword means that the function `test()` will always return a `promise`. Even if the function just executes a simple return 'Hello world'. It will be converted to a `promise` no matter what. That's why we can use a `then` to print the message.

The `await` keyword means that the code execution will stop until the `promise` returned by `job()` is resolved. And the return value from `test()` is stored in the `message` variable.


## Error handling

Use a simple try { ... } catch (error) { ... }. 

Look at the following example:

```javascript
function job() {
    return new Promise(function(resolve, reject) {
        setTimeout(reject, 500, 'Error happened');
    });
}

async function test() {
    try {
        let message = await job();
        console.log(message);

        return 'Hello world';
    } catch (error) {
        console.error(error);

        return 'Error happened during test';
    }
}

test()

.then(function(message) {
    console.log(message);
});

```

```
Error happened
Error happened during test
```

## Rejected promise in an async function

If you want to return a rejected promise in an async function, you just have to throw an error.

```javascript
async function job() {
    throw new Error("Access denied");
}

job()

.then(function(message) {
    console.log(message);
})

.catch(function(error) {
    console.log(error);
});

```

```
Error: Access denied
    at job ...
    at Object ...
    ...
```

## await can only be used in an async function

You can't use `await` outside of an async function. The code will just crash if you try. When you are coding in a NodeJS environment, that means that you can't use `await` in the body directly. You have to use a function like this:

```javascript
async function job() {
    return 'test'
}

async function main() {
    return await job()
}

main()

.then(data => console.log(data))
```

```
test
```

## You can await anything

You can use the `await` keyword even if the result is not a promise.

```javascript
const job = () => 'Salut'

const main = async() => {
    let message = await job()

    console.log(message)
}


main()
```

```
Salut
```


# NodeJS Challenges (Instructions) - Solving Problems  

## Async and Await

### Objectives

- To use `async/await` pattern.
- To use `Error Handling`.

### Description

Given the following implementation:


```javascript

//firstResult()
const firstResult = (err, data, callback) => {
    if (err) return callback(err)
    console.log(data)
    slowFunction((err, data) => secondResult(err, data, callback))
}

//secondResult()
const secondResult = (err, data, callback) => {
    if (err) return callback(err)
    console.log(data)
}

//fastFunction()
function fastFunction (done) {
    const a =  "Results of a",
          error = null
          //error = "Error in fastFunction"

    setTimeout(function () {
      done(error, a)
    }, 1000)
}

//slowFunction()
function slowFunction (done) {
    const b = "Results of b",
          error = "Error in slowFunction"

    setTimeout(function () {
      done(error, "Results of b")
    }, 3000)
}
  
// runSequentially()
function runSequentially (callback) {
    fastFunction((err, data) => firstResult(err, data, callback))
}


// driver code
runSequentially((err) => console.log(err))

```

We have the challenge to use asynchronous code using `await` and `async` and reach the following output:


When `fastFunction()` & `slowFunction()` are resolved:


```javascript
[
    'Results of a',
    'Results of b'
]
``` 

When there's an error in `fastFunction()`:
  
```
Error in fastFunction
```

## Deliverables

All tests must be true.

- When all services are resolved...


```javascript
runSequentially()

.then(data => {
    console.log("Test 1...", data.indexOf('Results of a') >= 0)
    console.log("Test 2...", data.indexOf('Results of b') >= 0)
})
 
.catch(e => console.log(e))

```

Output is...

```
Test 1... true
Test 2... true
```

- When Error in fastFunction()...

```javascript
runSequentially()

.then(data => {
    console.log("Test 1...", data.indexOf('Results of a') >= 0)
    console.log("Test 2...", data.indexOf('Results of b') >= 0)
})
 
.catch(e => console.log("Test 3...", e === "Error in fastFunction"))

```

Output is ...

```
Test 3... true
```


 
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />


> References:


1. [Async & Await Keywords](https://www.codingame.com/playgrounds/482/javascript-async-and-await-keywords/a-first-example)

2. [Asynchronous Javascript with Async and Await](https://nodejs.dev/learn/modern-asynchronous-javascript-with-async-and-await)
